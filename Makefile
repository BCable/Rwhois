all: Rpkg

clean:
	rm -f Rwhois_*.tar.gz
	rm -rf Rwhois.Rcheck

Rpkg:
	R CMD build .

check:
	R CMD check --as-cran Rwhois_*.tar.gz

devel-check:
	R-devel CMD check --as-cran Rwhois_*.tar.gz
